from python_challenge.enums import OperationEum, get_enum_values


def test_get_enum_values():
    operation_enums_list = ("LatestArticles",)

    assert get_enum_values(OperationEum) == operation_enums_list
