run_lint:
	poetry run tox -e lint

run_compliance:
	poetry run tox -e compliance

run_tests:
	poetry run pytest --cov=python_challenge --cov-fail-under=100 tests

setup_precommit:
	pre-commit install --hook-type pre-commit --hook-type pre-push --hook-type commit-msg
