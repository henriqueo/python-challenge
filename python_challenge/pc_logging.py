import logging
import logging.handlers


def get_logger(level=logging.INFO):
    logger = logging.getLogger(__name__)

    formatter = logging.Formatter(
        "%(asctime)s %(levelname)s %(pathname)s:%(lineno)d %(message)s"
    )

    rotating_file_handler = logging.handlers.RotatingFileHandler(
        filename="log.log",
        mode="a",
        maxBytes=1024 * 1024,
        backupCount=10,
        encoding="utf-8",
    )

    rotating_file_handler.setFormatter(formatter)

    stream_handler = logging.StreamHandler()

    stream_handler.setFormatter(formatter)

    logger.addHandler(rotating_file_handler)

    logger.addHandler(stream_handler)

    logger.setLevel(level)

    return logger
