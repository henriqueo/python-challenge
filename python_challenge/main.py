#!/usr/bin/env python3
import click  # pragma: no cover

from python_challenge import enums  # pragma: no cover
from python_challenge.client import Client  # pragma: no cover


@click.command()  # pragma: no cover
@click.option(
    "--no_pages", default=1, help="Number of pages.", type=int
)  # pragma: no cover
@click.option(
    "--operation",
    default=enums.OperationEum.LATEST_ARTICLES.value,
    help="Operation type.",
    type=click.Choice(
        enums.get_enum_values(enums.OperationEum), case_sensitive=False
    ),
)  # pragma: no cover
@click.option(
    "--edition",
    default=enums.EditionEnum.AFRICA.value,
    help="Edition name.",
    type=click.Choice(
        enums.get_enum_values(enums.EditionEnum), case_sensitive=False
    ),
)  # pragma: no cover
@click.option(
    "--no_articles_per_page",
    default=10,
    help="Number of articles per page.",
    type=int,
)  # pragma: no cover
@click.option("--offset", default="", help="Offset key.", type=str)
def main(
    no_pages, operation, edition, no_articles_per_page, offset
):  # pragma: no cover
    client = Client()

    operation_enum = [
        op for op in enums.OperationEum if op.value == operation
    ][0]

    edition_enum = [ed for ed in enums.EditionEnum if ed.value == edition][0]

    pages = client.get_articles(
        no_pages=no_pages,
        operation=operation_enum,
        edition=edition_enum,
        no_articles_per_page=no_articles_per_page,
        offset=offset,
    )

    for page_no, page in enumerate(pages):  # pragma: no cover
        page_no += 1

        click.echo("Page {}".format(page_no))

        for article_no, article in enumerate(page):  # pragma: no cover
            msg = "{article_no} - {title}: {link}".format(
                article_no=article_no + 1,
                title=article.title,
                link=article.link,
            )

            click.echo(msg)

        if page_no < no_pages:  # pragma: no cover
            click.echo("\n")


if __name__ == "__main__":  # pragma: no cover
    main()
