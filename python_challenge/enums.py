from enum import Enum, unique


@unique
class OperationEum(Enum):
    LATEST_ARTICLES = "LatestArticles"


@unique
class EditionEnum(Enum):
    AFRICA = "AFRICA"


def get_enum_values(enum):
    """
    Lists names of enum members.

    Args:
        enum (Enum): enum whose member's names are going to be listed.

    Yields:
        tuple: a list of names of enum's members.
    """

    return tuple(map(lambda x: x.value, enum))
